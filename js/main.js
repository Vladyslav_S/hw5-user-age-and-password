"use strict";

/* 1. Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
Экранирование - это инструменты для работы (изменения) строк. Нужны для редактирования полученых данных. */

function createNewUser() {
  const newUser = {
    firstName: "",
    lastName: "",
    birthday: new Date(),
    getAge() {
      return parseInt(
        (new Date() - this.birthday) / (1000 * 60 * 60 * 24 * 365)
      ); // проблема, не учитывает высокоснсый
    },
    getPassword() {
      return (
        this.firstName.substr(0, 1) +
        this.lastName.toLowerCase() +
        this.birthday.getFullYear()
      );
    },
    getLogin() {
      return (this.firstName.substring(0, 1) + this.lastName).toLowerCase();
    },
    setFirstName(value) {
      this.firstName = value;
    },
    setLastName(value) {
      this.lastName = value;
    },
    setAge(value) {
      const now = new Date();
      const dayOfBorn = value.substr(0, 2);
      const monthOfBorn = value.substr(3, 2);
      const yearOfBorn = value.substr(6, 4);

      if (
        dayOfBorn === null ||
        dayOfBorn.trim() === "" ||
        isNaN(+dayOfBorn) ||
        monthOfBorn === null ||
        monthOfBorn.trim() === "" ||
        isNaN(+monthOfBorn) ||
        yearOfBorn === null ||
        yearOfBorn.trim() === "" ||
        isNaN(+yearOfBorn)
      ) {
        alert("Error, wrong date!");
        return;
      }

      if (
        dayOfBorn < 0 ||
        dayOfBorn > 31 ||
        monthOfBorn < 0 ||
        monthOfBorn > 12 ||
        yearOfBorn < 1900 ||
        yearOfBorn > now.getFullYear()
      ) {
        alert("Error, wrong date!");
        return;
      }
      this.birthday = new Date(
        yearOfBorn + "-" + monthOfBorn + "-" + dayOfBorn
      );
      return;
    },
  };

  do {
    newUser.setFirstName(prompt("Enter your first name:", newUser.firstName));
    newUser.setLastName(prompt("Enter your last name", newUser.lastName));
    newUser.setAge(prompt("Enter your date of born:", "dd.mm.yyyy"));
  } while (
    newUser.firstName === null ||
    newUser.firstName === "" ||
    typeof newUser.firstName !== "string" ||
    newUser.lastName === null ||
    newUser.lastName === "" ||
    typeof newUser.lastName !== "string"
  );

  return newUser;
}

const user1 = createNewUser();
console.log(user1.getAge());
console.log(user1.getPassword());
